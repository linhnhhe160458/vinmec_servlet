<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
  <title>Homepage</title>
  <meta charset="UTF-8">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">

  <link rel="stylesheet" type="text/css" href="css/header.css">
  <link rel="stylesheet" type="text/css" href="css/homepage.css">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
        integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g=="
        crossorigin="anonymous" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css"
        integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw=="
        crossorigin="anonymous" />
</head>
<body>
<%--    import header--%>
<%@include file="Component/header.jsp"%>
<section class="banner">
  <div id="demo" class="carousel slide" data-bs-ride="carousel">
    <!-- Indicators/dots -->
    <div class="carousel-indicators promo-pic">
      <button type="button" data-bs-target="#demo" data-bs-slide-to="0" class="active"></button>
      <button type="button" data-bs-target="#demo" data-bs-slide-to="1"></button>
      <button type="button" data-bs-target="#demo" data-bs-slide-to="2"></button>
    </div>

    <!-- The slideshow/carousel -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="https://vinmec-prod.s3.amazonaws.com/images/vicaread/20220608_082319_123687_desktop.jpg"
             alt="Los Angeles" class="d-block" style="width:100%">
      </div>
      <div class="carousel-item">
        <img src="https://vinmec-prod.s3.amazonaws.com/images/vicaread/20220812_034852_575791_LA594_2-_banner_web-min_1.jpg"
             alt="Chicago" class="d-block" style="width:100%">
      </div>
      <div class="carousel-item">
        <img src="https://vinmec-prod.s3.amazonaws.com/images/vicaread/20220322_055811_527741_BANNER_TRUNG_TAM_FINAL_home_page_1.jpg"
             alt="New York" class="d-block" style="width:100%">
      </div>
    </div>

    <!-- Left and right controls/icons -->
    <button class="carousel-control-prev" type="button" data-bs-target="#demo" data-bs-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#demo" data-bs-slide="next">
      <span class="carousel-control-next-icon"></span>
    </button>
  </div>
  <!--    form book appointment-->
  <div class="book-appointment">
    <div class="container">
      <div class="col-xs-12 form-info">
        <h2>Để lại nhu cầu khám</h2>
        <form class="appoint-content">
          <div class="row form-area">
            <div class="col-md-4">
              <div class="form-name">
                Họ và tên <span>*</span><br>
                <input type="text">
              </div>
              <div class="form-phone">
                Số điện thoại <span>*</span> <br>
                <input type="text">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-birthdate">
                Ngày sinh <span>*</span> <br>
                <input type="input" id="datepicker">
                <img src="https://www.vinmec.com/static/img/icon/date-range-blue.5584c24fca2f.png"
                     class="date-icon">
              </div>
              <div class="form-address">
                Khu vực <span>*</span> <br>
                <select>
                  <option value="" selected="selected" hidden></option>
                  <option value="ha-noi">Hà Nội</option>
                  <option value="da-nang">Đà Nẵng</option>
                  <option value="nha-trang">Nha Trang</option>
                  <option value="hai-phong">Hải Phòng</option>
                  <option value="ha-long">Hạ Long</option>
                  <option value="phu-quoc">Phú Quốc</option>
                  <option value="ho-chi-minh">Hồ Chí Minh</option>
                </select>
                <img src="https://www.vinmec.com/static/img/icon/location-blue.fdcef5ae11e6.png"
                     class="location-icon">
              </div>
            </div>
            <div class="col-md-4 textarea-info">
              <label>Nhu cầu khám bệnh</label>
              <textarea class="ta-info" placeholder="Nhu cầu khám bệnh (không bắt buộc)"></textarea>
              <button class="button-info">ĐẶT LỊCH</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<section class="intro">
  <div class="container">
    <div class="col-xs-12">
      <p>Vinmec là Hệ thống y tế hàn lâm do Vingroup -<span> Tập đoàn kinh tế tư nhân hàng đầu Việt Nam </span>đầu
        tư phát triển với sứ mệnh
        "Chăm sóc bằng tài năng, y đức và sự thấu cảm".
      </p>
      <div class="counting-numbers">
        <div class="row">
          <div class="col-sm-3 col-xs-6 col-num">
            <div class="number">
              ~10
            </div>
            <div class="content">Năm kinh nghiệm</div>
          </div>
          <div class="col-sm-3 col-xs-6">
            <div class="number">
              02
            </div>
            <div class="content">bệnh viện được công nhận đạt chuẩn y tế toàn cầu JCI</div>
          </div>
          <div class="col-sm-3 col-xs-6">
            <div class="number">
              10
            </div>
            <div class="content">bệnh viện phòng khám</div>
          </div>
          <div class="col-sm-3 col-xs-6">
            <div class="number">
              400+
            </div>
            <div class="content">bác sĩ, chuyên gia trong và ngoài nước</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="expert">
  <div class="expert-intro">
    <div class="container">
      <div class="row">
        <div class="col-md-8 expert-header">
          <h2>Đội ngũ chuyên gia</h2>
          <p>Vinmec quy tụ đội ngũ chuyên gia, bác sĩ, dược sĩ và điều dưỡng được đào tạo bài bản đến chuyên
            sâu tại Việt nam
            và nhiều nước có nên y học phát triển như Mỹ, Anh, Pháp... Luôn lấy người bệnh là trung tâm,
            Vinmec cam kết mang lại dịch vụ chăm sóc sức khỏe toàn diện và chất lượng cao cho khách hàng.
          </p>
        </div>
        <div class="col-md-4">
          <div class="expert-search">

          </div>
        </div>
      </div>
    </div>
  </div>

  <div>
    <div>
      <div >
        <!-- Indicators/dots -->
        <div class="carousel-indicators promo-pic">
          <button type="button" data-bs-target="#demo1" data-bs-slide-to="0" class="active"></button>
          <button type="button" data-bs-target="#demo1" data-bs-slide-to="1"></button>
          <button type="button" data-bs-target="#demo1" data-bs-slide-to="2"></button>
        </div>

        <!-- The slideshow/carousel -->
        <div class="carousel-inner">
          <c:forEach items="${requestScope.listDoc1}" var="doc">
            <p>${doc.degree}</p>
          </c:forEach>
        </div>

        <!-- Left and right controls/icons -->
        <button class="carousel-control-prev" type="button" data-bs-target="#demo1" data-bs-slide="prev">
          <span class="carousel-control-prev-icon"></span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#demo1" data-bs-slide="next">
          <span class="carousel-control-next-icon"></span>
        </button>
      </div>
    </div>
  </div>

  <div class="expert-des">
    <div class="container">
      <div class="description">
        <p>Hoạt động không vì mục tiêu lợi nhuận, Vinmec luôn tiên phong trong ứng dụng các phương pháp điều trị mới nhất thế giới cùng chất lượng dịch vụ hoàn hảo để trở thành địa chỉ
          chăm sóc sức khỏe tiêu chuẩn quốc tế tại Việt Nam.</p>
        <button type="button">Đặt lịch khám ngay</button>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</section>

<section class="service">
  <div class="service-intro">
    <div class="container">
      <div class="col-8 header-intro">
        <h2>Dịch vụ nổi bật tại Vinmec</h2>
      </div>
      <div class="col-4 view-all">
        <a href="#">
          XEM TẤT CẢ
          <img src="https://www.vinmec.com/static/img/icon/plus-white-circle-blue.67b5ad6a7a53.svg">
        </a>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
  <div class="service-area">
    <div class="container">
      <div class="row first-line">
        <div class="col-8 left-side">
          <div class="row">
            <div class="col-xs-6 col-md-6 post-pic">
              <a href="#">
                <img src="https://vinmec-prod.s3.amazonaws.com/images/20220318_055214_956955_noi-soi-trong-xuong-l.max-800x800.jpg">
              </a>
            </div>
            <div class="col-xs-6 col-md-6 post-content">
              <h4>
                <a href="#" class="content-header">
                  Nội soi trong xương lấy u - Kỹ thuật mới lần đầu được triển khai tại Vinmec Times City
                </a>
              </h4>
              <p>
                Vừa qua, các chuyên gia tại Trung tâm Chấn thương chỉnh hình &amp; Y học thể thao, ...
              </p>
              <a href="#" class="button-detail">
                XEM CHI TIẾT
                <img src="https://www.vinmec.com/static/img/icon/plus-white.9adc61e69f70.svg">
              </a>
            </div>
            <div class="col-6 post-pic2">
              <a href="#">
                <img src="https://vinmec-prod.s3.amazonaws.com/images/20190713_094626_857428_noi-soi-can-thiep-cat.max-800x800.jpg">
              </a>
              <div class="post-title">
                <h4>Nội soi can thiệp sinh thiết niêm mạc ống tiêu hóa trên</h4>
              </div>
            </div>
            <div class="col-6 post-pic2">
              <a href="#">
                <img src="https://vinmec-prod.s3.amazonaws.com/images/20220315_095429_843360_LB086-_fb_1.max-800x800.jpg">
                <!--                                <img src="https://vinmec-prod.s3.amazonaws.com/images/20190713_094626_857428_noi-soi-can-thiep-cat.max-800x800.jpg">-->
              </a>
              <div class="post-title">
                <h4 class="post-name">
                  Ưu đãi 10% Gói khám hậu Covid-19
                </h4>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="col-4 right-side">
          <a href="#" class="pic-frame">
            <img src="https://vinmec-prod.s3.amazonaws.com/images/20210617_041659_992032_ghep_te_bao_goc_chua_.max-800x800.jpg">
          </a>
          <div class="service-name">
            <h4>
              <a href="#">Quy trình Ghép tế bào gốc điều trị bại não</a>
            </h4>
            <p class="intro-inside">
              70 - 80% bệnh nhân sau ghép tế bào gốc điều trị bại não có cải thiện về chức ...
            </p>
          </div>
        </div>
      </div>
      <div class="row second-line">
        <div class="col-8">
          <div class="row">
            <div class="col-6 post-content">
              <h4 class="post-title">
                <a href="#">
                  Gói thu thập, xử lý và lưu trữ Máu cuống rốn/Dây rốn
                </a>
              </h4>
              <p class="post-intro">
                Tế bào gốc có chức năng chuyên biệt để phát triển thành các mô, cơ quan khác ...
              </p>
              <a href="#" class="button-detail">
                Xem chi tiết
                <img src="https://www.vinmec.com/static/img/icon/plus-white.9adc61e69f70.svg">
              </a>
            </div>
            <div class="col-6 post2">
              <a href="#">
                <img src="https://vinmec-prod.s3.amazonaws.com/images/20210617_041743_627691_mau_cuong_ron.max-800.max-800x800.jpg">
              </a>
            </div>
          </div>
        </div>
        <div class="col-4 img-inside">
          <a class="post3" href="https://vinmec-prod.s3.amazonaws.com/images/20210617_041825_180096_Plasma_lanh_tri_seo.m.max-800x800.jpg">
            <img src="https://vinmec-prod.s3.amazonaws.com/images/20210617_041825_180096_Plasma_lanh_tri_seo.m.max-800x800.jpg">
          </a>
          <h4 class="post-title">
            <a href="#">
              Vinmec áp dụng Plasma lạnh - Công nghệ tiên tiến nhất hỗ trợ điều trị vết thương sau sin
            </a>
          </h4>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="equipment">
  <div class="container">
    <div class="row">
      <div class="col-9 equip-intro">
        <div class="row">
          <div class="col-5 equip-title">
            <h2>Trang thiết bị tối tân nhất thế giới</h2>
            <p>
              Sở hữu không gian khám chữa bệnh văn minh, sang trọng, hiện đại, Vinmec chú trọng đầu tư đồng bộ hệ thống trang thiết bị hiện đại hàng đầu thế giới, hỗ trợ hiệu quả cho việc chẩn đoán và điều trị.
            </p>
            <a href="#" class="button-detail">
              xem chi tiết
              <img src="https://www.vinmec.com/static/img/icon/plus-white.9adc61e69f70.svg">
            </a>
          </div>
          <div class="col-7 list-room">
            <h5>Phòng mổ Hybrid hiện đại nhất thế giới</h5>
            <ul>
              <li>•   Phòng Hybrid Discovery IGS 730 (của hãng GE Healthcare - Mỹ) đầu tiên tại Việt Nam</li>
            </ul>
            <h5>Máy chụp cắt lớp tiên tiến CT Ccan 640 lát</h5>
            <ul>
              <li>•  Tốc độ chụp nhanh, độ phân giải cao</li><li>•   Ứng dụng trong chụp mạch vành mà không phải sử dụng thuốc hạ nhịp tim, giảm liều tia xạ cho người bệnh.</li>
            </ul>
            <h5>Hệ thống SPECT/CT 16 dãy hiện đại</h5>
            <ul>
              <li>•   Sử dụng chất đánh dấu phóng xạ đặc hiệu để đánh giá chức năng tưới máu cơ tim, gan, thận,....</li>
            </ul>
            <h5>Máy xạ trị gia tốc Truebeam NDS120HD V2.7 (Mỹ)</h5>
            <ul>
              <li>• Trang bị công nghệ HyperArc, giúp việc xạ phẫu bằng máy gia tốc trở nên chính xác và rút ngắn.</li>
            </ul>
            <h5>Phòng Nội trú tiêu chuẩn quốc tế</h5>
            <ul>
              <li>• Phòng Tiêu chuẩn</li>
              <li>• Phòng VIP</li>
            </ul>
          </div>
        </div>
      </div>

      <div class="col-3 equip-pic">
        <img src="https://www.vinmec.com/static/img/state-of-the-art-equipment.69543a792b48.jpeg">
      </div>
    </div>
  </div>
</section>

<section class="block-wards">
  <div class="container">
    <div class="col-12">
      <h2 class="ward-title">Các chuyên khoa</h2>
      <p>Danh sách toàn bộ các Khoa &amp; Chuyên khoa tại bệnh viện đa khoa Vinmec</p>
      <p>this is the list</p>
      <div class="ward-name">
        <div class="owl-carousel owl-theme">
          <div class="item">
            <img src="https://vinmec-prod.s3.amazonaws.com/original_images/20210511_011442_039305_ungbuou.jpg">
            <p>Ung bướu - Xạ trị</p>
          </div>
          <div class="item">
            <img src="https://vinmec-prod.s3.amazonaws.com/original_images/20210617_041155_923235_Tieu_Hoa.jpg">
            <p>Tiêu hóa - Gan mật</p>
          </div>
          <div class="item">
            <img src="https://vinmec-prod.s3.amazonaws.com/original_images/20210617_041232_378211_Tim_mach.jpg">
            <p>Tim mạch</p>
          </div>
          <div class="item">
            <img src="https://vinmec-prod.s3.amazonaws.com/original_images/20210617_041308_626083_xuong.jpg">
            <p>Cơ xương khớp</p>
          </div>
          <div class="item">
            <img src="https://vinmec-prod.s3.amazonaws.com/original_images/20211229_092434_629713_Y_hoc_co_truyen.png">
            <p>Y Học Cổ Truyền</p>
          </div>
          <div class="item">
            <img src="https://vinmec-prod.s3.amazonaws.com/original_images/20210602_105501_128825_vacxin.png">
            <p>Vắc-xin</p>
          </div>
          <div class="item">
            <img src="https://vinmec-prod.s3.amazonaws.com/original_images/20210511_010527_200993_Nhi.jpg">
            <p>Nhi</p>
          </div>
          <div class="item">
            <img src="https://vinmec-prod.s3.amazonaws.com/original_images/20210602_105224_126010_Te_bao_goc.png">
            <p>Tế bào gốc và Công nghệ gen Vinmec</p>
          </div>
          <div class="item">
            <img src="https://vinmec-prod.s3.amazonaws.com/original_images/20210602_105333_368752_san_phu_khoa.png">
            <p>Sản phụ khoa và hỗ trợ sinh sản</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="block-news">
  <div class="container">
    <h2>Tin tức</h2>
    <a href="#">
      Xem tất cả
      <img src="https://www.vinmec.com/static/img/icon/plus-white-circle-blue.67b5ad6a7a53.svg">
    </a>

  </div>
</section>

<section class="my-vinmec">
  <div class="container">
    <div class="row">
      <div class="col-6 left-side">
        <h4 class="banner-intro">TẢI VỀ ỨNG DỤNG</h4>
        <h2 class="banner-title">MyVinmec</h2>
        <p class="banner-description">
          Ứng dụng chăm sóc sức khỏe độc quyền từ Vinmec
        </p>
        <div class="download-code">
          <a href="#">
            <img src="https://www.vinmec.com/static/onevinmec/imgs/apple-icon.abd363c16bb6.png">
            Tải về cho &nbsp<span>iOS</span>
          </a>
          <a href="#">
            <img src="https://www.vinmec.com/static/onevinmec/imgs/androi-icon.006cf5ba09aa.png">
            Tải về cho &nbsp<span>Android</span>
          </a>
          <div class="qr-code">
            <p>Hoặc quét mã QR code</p>
            <div class="code1">
              <img src="https://www.vinmec.com/static/onevinmec/imgs/apple.9fa9fc5407aa.svg">
            </div>
            <div class="code2">
              <img width="78" height="78" src="https://www.vinmec.com/static/onevinmec/imgs/androi.f9d534551580.svg">
            </div>
          </div>
        </div>
      </div>
      <div class="col-6 right-side">
        <img width="702" height="483" src="https://www.vinmec.com/static/img/mvm_destop_new.7dd70c573cdc.png">
      </div>
    </div>
  </div>
</section>
<section class="contact">
  <div class="container">
    <div class="content">
      <p>Tìm hiểu các sản phẩm dịch vụ được cung cấp độc quyền từ Vinmec</p>
      <a href="#">Khám phá ngay!</a>
      <div class="clearfix"></div>
    </div>
  </div>
</section>

<div class="sticky-icon">
  <div class="phone-icon">
    <div class="phone">
      <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAABNmlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarY6xSsNQFEDPi6LiUCsEcXB4kygotupgxqQtRRCs1SHJ1qShSmkSXl7VfoSjWwcXd7/AyVFwUPwC/0Bx6uAQIYODCJ7p3MPlcsGo2HWnYZRhEGvVbjrS9Xw5+8QMUwDQCbPUbrUOAOIkjvjB5ysC4HnTrjsN/sZ8mCoNTIDtbpSFICpA/0KnGsQYMIN+qkHcAaY6addAPAClXu4vQCnI/Q0oKdfzQXwAZs/1fDDmADPIfQUwdXSpAWpJOlJnvVMtq5ZlSbubBJE8HmU6GmRyPw4TlSaqo6MukP8HwGK+2G46cq1qWXvr/DOu58vc3o8QgFh6LFpBOFTn3yqMnd/n4sZ4GQ5vYXpStN0ruNmAheuirVahvAX34y/Axk/96FpPYgAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAB/ElEQVR42uya7W3CMBCG31QM4A1aNggTlG6QbpBMkHYC1AloJ4BOABuEDcgGtBOETnD9c1ERCH/lwxeaV8oPFGP86Hy+DxMREW5Bd7gRjSDSNGn4/RiAOvm8C0ZCRD5PSkQVXSr1nK/xE3mcWimA1ZV3JYBZCIO4giQANoYxMwYS6+xKY4lT5dJPreWZY+uspqSCKPYN27GJVBDXheVSQe494ksiEWTuMXcu1dld9SARxDX1OAJ4lgjy4zDnFsC076A4adEiRwAZg4hOUSpNoCsBPDGM+HqkNGynYBCuILuWj+dgWysGsNe8nwL4GsrW0m2fxZBq9rW0rNcX5MOQ9eZD8JFahcG5g/iKT671alGAYQggpYWvpEPYWrU/HDTOfeRIX0q2SL3QN4tGhZJukVobQyXYWw7WtLDKDIuM+ZSzscyCE9PCy5IttCvnZNaeiGLNHKuz8ZVh/MXTVu/1xQKmIqLEAuJ0fNo3iG5B51oSkeKnsBi/4bG9gYB/lCytU5G9DryFW+3Gm+JLwU7ehbJrwTjq4DJU8bHcVbEV9dXXqqP6uqO5e2/QZRYJpqu2IUAA4B3tXvx8hgKp05QZW6dJqrLTNkB6vrRURLRwPHqtYgkC3cLWQAcDQGGKH13FER/NATzi786+BPDNjm1dMkfjn2pGkBHkf4D8DgBJDuDHx9BN+gAAAABJRU5ErkJggg==">
    </div>
  </div>
  <div class="col-6 message-icon">
    <div class="message">
      <img src="message-icon.jpg">
    </div>
  </div>
</div>
</body>
</html>
