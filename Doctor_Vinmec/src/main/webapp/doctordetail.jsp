
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <!-- font aw-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">

    <link rel="stylesheet" type="text/css" href="css/header.css">
    <link rel="stylesheet" type="text/css" href="css/doctordetail.css">
    <link rel="stylesheet" type="text/css" href="../css/FooterStyle.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
          integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g=="
          crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css"
          integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw=="
          crossorigin="anonymous" />
    <title>Doctor Detail</title>
</head>
<body>
<%@include file="Component/header.jsp"%>
<section class="intro-banner">
    <div class="container banner-top">
        <img src="${requestScope.doctor.image}">
        <div class="info-intro">
            <div class="top-breadcrumb">
                <i aria-hidden="true" class="fa fa-home"></i>
                <span>Bác sĩ > ${requestScope.doctor.speciality}</span>
                <h2>
                    ${requestScope.doctor.title} ${requestScope.doctor.degree} ${requestScope.doctor.doctorName}
                </h2>
            </div>
        </div>
    </div>
    <div class="appointment">
        <div class="container wrap-column">
            <div class="column detail">
                <span> Đăng ký khám</span>
            </div>
            <div class="column2 detail">
                <span>Nhận xét</span>
            </div>
            <div class="column detail">
                <span>Thông tin chi tiết</span>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<section class="body-content">
    <div class="container">
        <div class="row">
            <div class="col-4 intro">
                <div class="header collapsible-header" data-bs-toggle="collapse" href="#collapseExample">
                    <i aria-hidden="true" class="fa fa-fw fa-info-circle"></i>
                    GIỚI THIỆU
                </div>
                <div id="collapseExample" class="wrapper-div collapse intro-content card-body">
                    <p>${requestScope.doctor.intro}</p>
                    <a href="#">
                        Xem đầy đủ
                        <i class="fa fa-angle-double-right link-article"></i>
                    </a>
                </div>
            </div>
            <div class="col-4 center-info">
                <div class="distance doc-position">
                    <div class="header collapsible-header" data-bs-toggle="collapse" href="#collapseExamplePos">
                        <i aria-hidden="true" class="fa fa-fw fa-briefcase"></i>
                        CHỨC VỤ
                    </div>
                    <div id="collapseExamplePos" class="wrapper-div div-content collapse intro-content card-body">
                        <p class="bold-text">${requestScope.position}</p>
                    </div>
                </div>
                <div class="distance workplace">
                    <div class="header collapsible-header" data-bs-toggle="collapse" href="#collapseExampleWorkplace">
                        <i aria-hidden="true" class="fa fa-fw fa-hospital-o"></i>
                        NƠI CÔNG TÁC
                    </div>
                    <div id="collapseExampleWorkplace" class="wrapper-div workplace-body">
                        <a href="#">
                            <img src="https://vinmec-prod.s3.amazonaws.com/images/07_08_2019_07_25_17_135275.jpeg">
                            <span>${requestScope.doctor.workplace}</span>
                            <div class="clearfix"></div>
                        </a>
                    </div>
                </div>
                <div class="distance experience">
                    <div class="header collapsible-header" data-bs-toggle="collapse" href="#collapseExampleEx">
                        <i aria-hidden="true" class="fa fa-fw fa-hospital-o"></i>
                        KINH NGHIỆM
                    </div>
                    <div id="collapseExampleEx" class="wrapper-div ex-body">
                        <ul>
                            <li>Tại Hà Nội:</li>
                            <li>Trưởng khoa Gây mê giảm đau bệnh viện Đa khoa Quốc tế Vinmec Times City</li>
                            <li>Giáo sư Dự án Đại học Y VinUni – Khối Khoa học sức khỏe</li>
                            <li>Tại Dubai:</li>
                            <li>Cố vấn Gây mê và điều trị đau can thiệp tại Bệnh viện Chính phủ Rashid</li>
                            <li>Giám đốc Chuyên môn, Trung tâm Cấp cứu và Khoa Ngoại Bệnh viện Dubai London</li>
                            <li>Tại Pháp:</li>
                            <li>Bác sĩ Gây mê, cố vấn cấp cao tại Trung tâm Clinical Angouleme - Trung tâm kỹ thuật cao về Gây tê giảm đau vùng và Chăm sóc giảm nhẹ</li>
                            <li>Cố vấn sản khoa với các trường hợp nguy cơ rủi ro cao, cộng sự của Giáo sư. JM GONNET</li>
                        </ul>
                    </div>
                </div>
                <div class="distance awards">
                    <div class="header">
                        <i aria-hidden="true" class="fa fa-fw fa-trophy"></i>
                        GIẢI THƯỞNG VÀ GHI NHẬN
                    </div>
                </div>
                <div class="distance works-books">
                    <div class="header">
                        <i aria-hidden="true" class="fa fa-fw fa-book"></i>
                        SÁCH, BÁO, CÔNG TRÌNH NGHIÊN CỨU
                    </div>
                </div>
                <div class="distance related-articles">
                    <div class="header">
                        <i aria-hidden="true" class="fa fa-fw fa-star-o"></i>
                        CÁC BÀI VIẾT LIÊN QUAN (14)
                    </div>
                </div>
            </div>
            <div class="col-4 rightside-info">
                <div class="distance speciality">
                    <div class="header">
                        <i aria-hidden="true" class="fa fa-fw fa-stethoscope"></i>
                        CHUYÊN KHOA
                    </div>
                </div>
                <div class="distance service">
                    <div class="header">
                        <i aria-hidden="true" class="fa fa-fw fa-list"></i>
                        DỊCH VỤ
                    </div>
                </div>
                <div class="distance education">
                    <div class="header">
                        <i aria-hidden="true" class="fa fa-fw fa-graduation-cap"></i>
                        QUÁ TRÌNH ĐÀO TẠO
                    </div>
                </div>
                <div class="distance member">
                    <div class="header">
                        <i aria-hidden="true" class="fa fa-fw fa-users"></i>
                        THÀNH VIÊN CỦA CÁC TỔ CHỨC
                    </div>
                </div>
                <div class="distance language">
                    <div class="header collapsible-header" data-bs-toggle="collapse" href="#collapseLang">
                        <i aria-hidden="true" class="fa fa-fw fa-globe"></i>
                        NGÔN NGỮ
                    </div>
                    <div id="collapseLang" class="wrapper-div collapse intro-content card-body">
                        <p>${requestScope.doctor.language}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="sticky-icon">
    <div class="phone-icon">
        <div class="phone">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAABNmlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarY6xSsNQFEDPi6LiUCsEcXB4kygotupgxqQtRRCs1SHJ1qShSmkSXl7VfoSjWwcXd7/AyVFwUPwC/0Bx6uAQIYODCJ7p3MPlcsGo2HWnYZRhEGvVbjrS9Xw5+8QMUwDQCbPUbrUOAOIkjvjB5ysC4HnTrjsN/sZ8mCoNTIDtbpSFICpA/0KnGsQYMIN+qkHcAaY6addAPAClXu4vQCnI/Q0oKdfzQXwAZs/1fDDmADPIfQUwdXSpAWpJOlJnvVMtq5ZlSbubBJE8HmU6GmRyPw4TlSaqo6MukP8HwGK+2G46cq1qWXvr/DOu58vc3o8QgFh6LFpBOFTn3yqMnd/n4sZ4GQ5vYXpStN0ruNmAheuirVahvAX34y/Axk/96FpPYgAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAB/ElEQVR42uya7W3CMBCG31QM4A1aNggTlG6QbpBMkHYC1AloJ4BOABuEDcgGtBOETnD9c1ERCH/lwxeaV8oPFGP86Hy+DxMREW5Bd7gRjSDSNGn4/RiAOvm8C0ZCRD5PSkQVXSr1nK/xE3mcWimA1ZV3JYBZCIO4giQANoYxMwYS6+xKY4lT5dJPreWZY+uspqSCKPYN27GJVBDXheVSQe494ksiEWTuMXcu1dld9SARxDX1OAJ4lgjy4zDnFsC076A4adEiRwAZg4hOUSpNoCsBPDGM+HqkNGynYBCuILuWj+dgWysGsNe8nwL4GsrW0m2fxZBq9rW0rNcX5MOQ9eZD8JFahcG5g/iKT671alGAYQggpYWvpEPYWrU/HDTOfeRIX0q2SL3QN4tGhZJukVobQyXYWw7WtLDKDIuM+ZSzscyCE9PCy5IttCvnZNaeiGLNHKuz8ZVh/MXTVu/1xQKmIqLEAuJ0fNo3iG5B51oSkeKnsBi/4bG9gYB/lCytU5G9DryFW+3Gm+JLwU7ehbJrwTjq4DJU8bHcVbEV9dXXqqP6uqO5e2/QZRYJpqu2IUAA4B3tXvx8hgKp05QZW6dJqrLTNkB6vrRURLRwPHqtYgkC3cLWQAcDQGGKH13FER/NATzi786+BPDNjm1dMkfjn2pGkBHkf4D8DgBJDuDHx9BN+gAAAABJRU5ErkJggg==">
        </div>
    </div>
    <div class="col-6 message-icon">
        <div class="message">
            <img src="message-icon.jpg">
        </div>
    </div>
</div>
</body>
</html>
