<%--
  Created by IntelliJ IDEA.
  User: Halinh
  Date: 02/10/2022
  Time: 10:50 CH
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Header</title>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
<%--    <link rel="stylesheet" type="text/css" href="css/header.css">--%>
</head>
<body>
<section class="language">
    <div class="container">
        <div class="row">
            <div class="col-11">
                <ul class="sub-menu">
                    <li>Tải ứng dụng MyVinmec</li>
                    <li>Hướng dẫn khách hàng</li>
                    <li>Hệ thống Vinmec</li>
                    <li>Cổng chăm sóc khách hàng</li>
                </ul>
            </div>
            <div class="col-1">
                <select name="languages">
                    <option value="viet">
                        Tiếng Việt
                    </option>
                    <option value="eng">English</option>
                </select>
            </div>
        </div>
    </div>
</section>

<section class="middle">
    <div class="container">
        <div class="row">
            <div class="col-8 search-info">
                <div class="logo">
                    <a href="Index.html"><img src="https://www.vinmec.com/static/img/logo.9e7e5ef03cbd.svg" height="50"
                                              class="logo-vinmec">
                    </a>
                </div>
                <div class="location">
                    <select name="location" class="check-location">
                        <option>Hải Phòng</option>
                        <option>Hà Nội</option>
                        <option>Hồ Chí Minh</option>
                        <option>Nha Trang</option>
                        <option>Hạ Long</option>
                        <option>Phú Quốc</option>
                    </select>
                </div>
                <div class="search">
                    <i class="fa-solid fa-magnifying-glass"></i>
                    <input type="text" placeholder="Tìm kiếm bài viết, trợ giúp...">
                </div>
            </div>
            <div class="col-4 two-logos">
                <div class="logo-doctor">
                    <img src="https://www.vinmec.com/static/build/cbe6a520989f0a627e43430e550f4895.svg">
                    <span>HỎI BÁC SĨ VINMEC</span><br>
                    <i>Phí dịch vụ: 300.000 VND</i>
                </div>
                <div class="logo-doctor">
                    <span>Đăng ký khám</span>
                    <img src="https://www.vinmec.com/static/build/abfdfc8824997f7e9471d120546c71e8.svg">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bottom">
    <div class="container">
        <div class="row menu-item">
            <div class="col-11">
                <ul class="menu">
                    <li>Hệ thống Vinmec &#8595;
                        <ul class="small-items other">
                            <li><a href="#">Vinmec Central Park (Tp.Hồ Chí Minh)</a></li>
                            <li><a href="#">Vinmec Times City (Hà Nội)</a></li>
                            <li><a href="#">Vinmec Đà Nẵng (Đà Nẵng)</a></li>
                            <li><a href="#">Vinmec Nha Trang (Khánh Hòa)</a></li>
                            <li><a href="#">Vinmec Hải Phòng (Hải Phòng)</a></li>
                            <li><a href="#">Vinmec Hạ Long (Quảng Ninh)</a></li>
                            <li><a href="#">Vinmec Phú Quốc (Kiên Giang)</a></li>
                            <li><a href="#">Vinmec Hạ Long (Quảng Ninh)</a></li>
                            <li><a href="#">Bệnh viện & Phòng khám</a></li>
                            <li><a href="#">Cơ sở vật chất</a></li>
                        </ul>
                    </li>
                    <li>Chuyên khoa trọng điểm &#8595;
                        <ul class="other">
                            <li><a href="#">Sản phụ khoa và hỗ trợ sinh sản</a></li>
                            <li><a href="#">Tim mạch</a></li>
                            <li><a href="#">Nhi</a></li>
                            <li><a href="#">Ung bướu - Xạ trị</a></li>
                            <li><a href="#">Sức khoẻ tổng quát</a></li>
                            <li><a href="#">Tiêu hóa - Gan mật</a></li>
                            <li><a href="#">Cơ xương khớp</a></li>
                            <li><a href="#">Tế bào gốc và Công nghệ gen</a></li>
                            <li><a href="#">Ngân hàng Mô Vinmec</a></li>
                            <li><a href="#">Y Học Cổ Truyền</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="doctorList">Danh sách bác sĩ</a>

                    </li>
                    <li>Gói dịch vụ &#8595;
                        <ul class="other">
                            <li><a href="#">Thai sản trọn gói</a></li>
                            <li><a href="#">Gói sàng lọc giáp</a></li>
                            <li><a href="#">Bảo hiểm & Chương trình hội viên</a></li>
                            <li><a href="#">Sàng lọc ung thư</a></li>
                            <li><a href="#">Khám sức khỏe tổng quát</a></li>
                        </ul>
                    </li>
                    <li>Hướng dẫn khách hàng &#8595;
                        <ul class="other">
                            <li><a href="#">Thai sản trọn gói</a></li>
                            <li><a href="#">Gói sàng lọc giáp</a></li>
                            <li><a href="#">Bảo hiểm & Chương trình hội viên</a></li>
                            <li><a href="#">Sàng lọc ung thư</a></li>
                            <li><a href="#">Khám sức khỏe tổng quát</a></li>
                        </ul>
                    </li>
                    <li>Khác &#8595;
                        <ul class="other">
                            <li><a href="#">Giới thiệu</a></li>
                            <li><a href="#">Thông tin Dược</a></li>
                            <li><a href="#">Tin Tức</a></li>
                            <li><a href="#">Ngân hàng mô</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col-1">
                <div class="row">
                    <div class="col-6">
                        <a href="#" class="media fb">
                            <img src="https://www.vinmec.com/static/img/icon/facebook-black-circle.28fdd10f657c.png"
                                 height="30">
                        </a>
                    </div>
                    <div class="col-6">
                        <a href="#" class="media youtube">
                            <img src="https://www.vinmec.com/static/img/icon/youtube-black-circle.7e2ca6495a65.png"
                                 height="30">
                        </a>
                        </li>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>
