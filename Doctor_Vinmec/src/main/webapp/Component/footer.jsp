
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
<%--    <link rel="stylesheet" type="text/css" href="../css/footer.css">--%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
          integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g=="
          crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css"
          integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw=="
          crossorigin="anonymous" />
    <title>Footer</title>
</head>
<body>
<footer>
    <section class="first-part">
        <div class="container">
            <div class="row">
                <div class="col-4 subscribe">
                    <h3>Theo dõi tin tức và dịch vụ mới nhất của chúng tôi!</h3>
                    <div class="email-subscription">
                        <div class="email-input">
                            <input type="text" placeholder="Địa chỉ email" class="subcribe-input">
                            <button class="btn-subscribe" id="subscribe-submit">ĐĂNG KÝ</button>
                        </div>
                        <div class="privacy-policy">
                        <span>Bằng cách nhấn nút Đăng ký hoặc nút gửi thông tin đi, tôi xác nhận đã đọc và đồng ý với các Quy định
                            <a href="#"><b>Chính sách quyền riêng tư</b></a>
                        </span>
                        </div>
                        <div class="social">
                            <h3>Theo dõi chúng tôi</h3>
                            <div class="social-icon">
                                <div>
                                    <a href="#">
                                        <img src="https://www.vinmec.com/static/img/icon/Facebook.svg">
                                    </a>
                                </div>
                                <div>
                                    <a href="#">
                                        <img src="https://www.vinmec.com/static/img/icon/Youtube.svg">
                                    </a>
                                </div>
                            </div>
                            <div class="logo-outline">
                                <a href="#">
                                    <img src="https://www.vinmec.com/static/img/logoSaleNoti.png">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row col-5 about">
                    <div class="col-6 footer-menu">
                        <ul class="menu-list">
                            <h3>VỀ CHÚNG TÔI</h3>
                            <li><a href="#">Hệ thống Vinmec</a></li>
                            <li><a href="#">Đội ngũ bác sĩ</a></li>
                            <li><a href="#">Tin tức</a></li>
                            <li><a href="#">Tuyển dụng</a></li>
                            <li><a href="#">Sitemap</a></li>
                            <li><a href="#">Chính sách quyền riêng tư</a></li>
                        </ul>
                    </div>
                    <div class="col-6 footer-menu">
                        <ul class="menu-list">
                            <h3>DỊCH VỤ VINMEC</h3>
                            <li><a href="#">Giới thiệu</a></li>
                            <li><a href="#">Đăng ký khám và tư vấn</a></li>
                            <li><a href="#">Gói dịch vụ</a></li>
                            <li><a href="#">Hướng dẫn khách hàng</a></li>
                            <li><a href="#">Viện nghiên cứu Tế bào gốc và Công nghệ Gen Vinmec</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-3 download-app">
                    <h3>
                        <a href="#">Tải ứng dụng MyVinmec</a>
                    </h3>
                    <div class="download-qr-code">
                        <a href="#" class="app-logo">
                            <img src="https://www.vinmec.com/static/onevinmec/imgs/androi.svg">
                        </a>
                    </div>
                    <div class="download-logo">
                        <a href="#" class="">
                            <img src="https://www.vinmec.com/static/onevinmec/imgs/apple-icon.png">
                            App Store
                        </a>
                        <a href="#">
                            <img src="https://www.vinmec.com/static/onevinmec/imgs/androi-icon.png">
                            Google Play
                        </a>
                        <div class="clearfix"></div>
                    </div>

                    <div class="public-health">
                        <h4>ĐỐI TÁC LIÊN KẾT</h4>
                        <a href="#">
                            <img src="https://www.vinmec.com/static/img/logoyhoccongdong.png">
                        </a>
                        <a href="#">
                            <img src="https://roche-h.assetsadobe2.com/is/image/content/dam/roche-pacientes-2/vi_vn/assets/logo/BUOCTIEP%20Logo.png?wid=1920&fit=constrain,0&fmt=png-alpha">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <hr>
    <section class="copyright">
        <div class="container">
            <p>Copyright © 2019 Vinmec. All rights reserved</p>
            <p class="disclaimer">
                Các thông tin trên website Vinmec.com chỉ dành cho mục đích tham khảo, tra cứu, khuyến nghị Quý khách hàng không tự ý áp dụng.
                Vinmec không chịu trách nhiệm về những trường hợp tự ý áp dụng mà không có chỉ định của bác sĩ.
            </p>
        </div>
    </section>

    <section class="vin3s">
        <div class="container about-us">
            <div class="contact-now">
                <h4>Công ty Cổ phần<br>
                    Bệnh viện Đa khoa Quốc tế Vinmec
                </h4>
                <a href="#">Liên Hệ Ngay</a>
            </div>
            <div class="biz-number">
                <p class="icon-register">Số đăng ký kinh doanh: 0106050554</p>
                <p>do Sở Kế hoạch và Đầu tư Thành phố Hà Nội cấp lần đầu ngày 30 tháng 11 năm 2012</p>
            </div>
            <div class="address">
              <span>
                Địa chỉ công ty: số 458, phố Minh Khai, Phường Vĩnh Tuy, Quận Hai Bà Trưng, Thành phố Hà Nội, Việt Nam
              </span>
            </div>
        </div>
    </section>
</footer>
</body>
</html>
