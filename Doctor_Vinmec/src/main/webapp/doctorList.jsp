<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="css/header.css">
    <link rel="stylesheet" type="text/css" href="css/doctorlist.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <title>Doctor list</title>
</head>
<body>
<%--<%@include file="Component/header.jsp" %>--%>
<section class="page-name">
    <div class="container title-part">
        <h4>Trang chủ \ Bác sĩ</h4>
        <h1>
            Danh sách bác sĩ
            <span>trên cả nước (trang 1)</span>
        </h1>
    </div>
</section>

<section class="filter-doctor">
    <div class="container body-row">
        <div class="doctor-area">
            <div class="doctor-detail">
                <div class="row">
                    <div class="col-8">
                        <div class="filter-doctor">
                            <div class="filter-button">
                                <form action="doctorList" method="get">
                                    <a href="#">
                                        Tỉnh thành
                                        <ul class="row">
                                            <c:forEach items="${requestScope.listLocation}" var="loc">
                                                <li class="col-4">
                                                    <input type="checkbox" value="${loc.locationCode}" name="location">
                                                        ${loc.locationName}
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </a>
                                    <a href="#">
                                        Chuyên khoa
                                        <ul class="row">
                                            <c:forEach items="${requestScope.listSpec}" var="spec">
                                                <li class="col-4">
                                                    <input type="checkbox" value="${spec.getSpecid()}" name="speciality">
                                                        ${spec.specName}
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </a>
                                    <a href="#">
                                        Nghề nghiệp
                                    </a>
                                    <a href="#">
                                        Ngôn ngữ
                                        <ul class="row">
                                            <c:forEach items="${requestScope.listLang}" var="lang">
                                                <li class="col-4">
                                                    <input type="checkbox" value="${lang.language}" name="language">
                                                        ${lang.language}
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </a>
                                    <a href="#">
                                        Nơi đào tạo
                                    </a>
                                    <a href="#">
                                        Học hàm
                                        <ul class="row shorter-ul">
                                            <c:forEach items="${requestScope.listTitle}" var="tt" begin="1" end="${requestScope.listTitle.size()}">
                                                <li class="col-4">
                                                    <input type="checkbox" value="${tt.title}" name="title">
                                                        ${tt.title}
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </a>
                                    <a href="#">
                                        Học vị
                                        <ul class="row shorter-ul">
                                            <c:forEach items="${requestScope.listDegree}" var="deg">
                                                <li class="col-4">
                                                    <input type="checkbox" value="${deg.degreeName}" name="degree">
                                                        ${deg.degreeName}
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </a>
                                    <div class="filter-button-submit">
                                        <button type="submit" class="button">Filter</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <c:forEach items="${requestScope.listDoc}" var="doc">
                            <div class="doctor-card">
                                <div class="row">
                                    <div class="col-2 media">
                                        <a href="#">
                                            <img src="${doc.image}">
                                        </a>
                                    </div>
                                    <div class="col-5 doc-info">
                                        <a href="doctordetail?docid=${doc.doctorID}">
                                            <h2>${doc.doctorName}</h2>
                                        </a>
                                        <div>
                                            <i class="fa fa-graduation-cap"></i>
                                            <a href="#">${doc.title} ${doc.degree} </a>
                                            <br>
                                            <i class="fa fa-stethoscope"></i>
                                            <a href="#">${doc.speciality}</a>
                                            <br>
                                            <i class="fa fa-hospital-o"></i>
                                            <a href="#">${doc.workplace}</a>
                                        </div>
                                    </div>
                                    <div class="col-5 doc-description">
                                        <div>
                                           ${doc.intro}
                                           <a href="#">Xem thêm </a>
                                          <i class="fa fa-angle-double-right"></i>
                                        </div>

                                        <button>
                                            <i class="fa fa-calendar-check-o"></i>
                                            Đăng ký khám
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                        <div class="pagination">
                            <div class="page-number">
                                <c:if test="${requestScope.pageIndex > 1}">
                                    <a href="${pageContext.request.contextPath}/doctorList?page=${requestScope.pageIndex-1}&${requestScope.url}">Trước
                                        <i class="fa fa-chevron-left"></i></a>
                                </c:if>
                                <span>Trang ${requestScope.pageIndex} / ${requestScope.totalPage} </span>
                                <c:if test="${requestScope.pageIndex < requestScope.totalPage}">
                                    <a id ="next" href="">Sau
                                        <i class="fa fa-chevron-right"></i>
                                    </a>
                                </c:if>
                            </div>
                        </div>
                    </div>
                    <div class="col-4 sidebar">
                        <div class="departments">
                            <h3>Chuyên khoa</h3>
                            <ul>
                                <c:forEach items="${requestScope.listDocSpec}" var="ds" begin="0" end="15">
                                    <li>
                                        <a href="speciality?spec=${ds.getSpecid()}">
                                            <span class="dept-name">${ds.getSpecName()}</span>
                                            <i class="fa fa-user-md"></i>
                                            <span class="dept-number">${ds.doctorAmount}</span>
                                            <span class="clearfix"></span>
                                        </a>
                                    </li>
                                </c:forEach>
                            </ul>
                            <div class="expand">
                                <i class="fa fa-chevron-down"></i>
                            </div>
                        </div>
                        <div class="useful-info">
                            <h3>Thông tin hữu ích</h3>
                            <ul>
                                <li>
                                    <div class="row">
                                        <div class="col-3 img-info">
                                            <a href="#">
                                                <img src="https://vinmec-prod.s3.amazonaws.com/images/20220715_083454_918986_291524463_60631533803.max-110x110.jpg">
                                            </a>
                                        </div>
                                        <div class="col-7 info-body">
                                            <a href="#">
                                                Trợ lý sức khỏe thông minh - Ứng dụng an toàn cho cả gia đình bạn!
                                            </a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-3 img-info">
                                            <a href="#">
                                                <img src="https://vinmec-prod.s3.amazonaws.com/images/20220427_052357_350346_kham-suc-khoe-tu-xa.max-110x110.jpg">
                                            </a>
                                        </div>
                                        <div class="col-7 info-body">
                                            <a href="#">
                                                Trợ lý sức khỏe thông minh - Ứng dụng an toàn cho cả gia đình bạn!
                                            </a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-3 img-info">
                                            <a href="#">
                                                <img src="https://vinmec-prod.s3.amazonaws.com/images/20220427_043359_909968_dat-hen-cham-soc-suc-.max-110x110.jpg">
                                            </a>
                                        </div>
                                        <div class="col-7 info-body">
                                            <a href="#">Triển khai Dịch vụ Chăm sóc sức khỏe từ xa trên ứng dụng
                                                MyVinmec</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-3 img-info">
                                            <a href="#">
                                                <img src="https://vinmec-prod.s3.amazonaws.com/images/20220427_051225_738287_app-my-vinmec-1.max-110x110.jpg">
                                            </a>
                                        </div>
                                        <div class="col-7 info-body">
                                            <a href="#">
                                                Trợ lý sức khỏe thông minh - Ứng dụng an toàn cho cả gia đình bạn!
                                            </a>
                                        </div>
                                    </div>

                                </li>
                            </ul>
                            <div class="expand">
                                <a href="#">Xem tất cả thông tin</a>
                                <i class="fa fa-chevron-down"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="sticky-icon">
    <div class="phone-icon">
        <div class="phone">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAABNmlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarY6xSsNQFEDPi6LiUCsEcXB4kygotupgxqQtRRCs1SHJ1qShSmkSXl7VfoSjWwcXd7/AyVFwUPwC/0Bx6uAQIYODCJ7p3MPlcsGo2HWnYZRhEGvVbjrS9Xw5+8QMUwDQCbPUbrUOAOIkjvjB5ysC4HnTrjsN/sZ8mCoNTIDtbpSFICpA/0KnGsQYMIN+qkHcAaY6addAPAClXu4vQCnI/Q0oKdfzQXwAZs/1fDDmADPIfQUwdXSpAWpJOlJnvVMtq5ZlSbubBJE8HmU6GmRyPw4TlSaqo6MukP8HwGK+2G46cq1qWXvr/DOu58vc3o8QgFh6LFpBOFTn3yqMnd/n4sZ4GQ5vYXpStN0ruNmAheuirVahvAX34y/Axk/96FpPYgAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAB/ElEQVR42uya7W3CMBCG31QM4A1aNggTlG6QbpBMkHYC1AloJ4BOABuEDcgGtBOETnD9c1ERCH/lwxeaV8oPFGP86Hy+DxMREW5Bd7gRjSDSNGn4/RiAOvm8C0ZCRD5PSkQVXSr1nK/xE3mcWimA1ZV3JYBZCIO4giQANoYxMwYS6+xKY4lT5dJPreWZY+uspqSCKPYN27GJVBDXheVSQe494ksiEWTuMXcu1dld9SARxDX1OAJ4lgjy4zDnFsC076A4adEiRwAZg4hOUSpNoCsBPDGM+HqkNGynYBCuILuWj+dgWysGsNe8nwL4GsrW0m2fxZBq9rW0rNcX5MOQ9eZD8JFahcG5g/iKT671alGAYQggpYWvpEPYWrU/HDTOfeRIX0q2SL3QN4tGhZJukVobQyXYWw7WtLDKDIuM+ZSzscyCE9PCy5IttCvnZNaeiGLNHKuz8ZVh/MXTVu/1xQKmIqLEAuJ0fNo3iG5B51oSkeKnsBi/4bG9gYB/lCytU5G9DryFW+3Gm+JLwU7ehbJrwTjq4DJU8bHcVbEV9dXXqqP6uqO5e2/QZRYJpqu2IUAA4B3tXvx8hgKp05QZW6dJqrLTNkB6vrRURLRwPHqtYgkC3cLWQAcDQGGKH13FER/NATzi786+BPDNjm1dMkfjn2pGkBHkf4D8DgBJDuDHx9BN+gAAAABJRU5ErkJggg==">
        </div>
    </div>
</div>
<jsp:include page="Component/footer.jsp"/>
</body>

<script>
    var params = window.location.search;
    var currentLink = window.location;
    var pageIndex = new URLSearchParams(params).get("page");
    console.log("current link: "+currentLink);
    if(pageIndex == null){
        if(currentLink.href.includes("?")){
            nextLink = currentLink+"&page=2";
        }else{
            nextLink = currentLink+"?page=2";
        }
    }else{
        var pageLink = window.location.href;
        console.log("current page link: "+pageLink);
        nextLink = pageLink.replace("page="+pageIndex,"page="+(parseInt(pageIndex)+1));
    }

    console.log("NEXT: "+nextLink);
    document.getElementById("next").setAttribute("href",nextLink);
</script>
</html>
