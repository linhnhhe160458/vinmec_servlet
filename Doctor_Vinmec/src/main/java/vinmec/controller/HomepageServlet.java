package vinmec.controller;

import dal.DoctorDAO;
import model.Doctor;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "HomepageServlet", value = "/home")
public class HomepageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DoctorDAO doctorDAO = new DoctorDAO();
        ArrayList<Doctor> listDoctor1 = doctorDAO.getDoctorHomePage();
//        System.out.println(listDoctor1.size());
        request.setAttribute("listDoc1",listDoctor1);
        request.getRequestDispatcher("homepage.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
