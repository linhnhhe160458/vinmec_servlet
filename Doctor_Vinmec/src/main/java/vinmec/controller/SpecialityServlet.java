package vinmec.controller;

import dal.DoctorDAO;
import model.Doctor;
import model.Speciality;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "SpecialityServlet", value = "/speciality")
public class SpecialityServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DoctorDAO doctorDAO = new DoctorDAO();
        String spec = "Nhi";
        ArrayList<Speciality> listDoctorSpec = doctorDAO.listDoctorSpecSidebar(); //get list on the side bar
        int pageindex = 1;
        int totalPage = 1;
        if (request.getParameter("spec") != null) {
            spec = request.getParameter("spec");
            if (request.getParameter("page") != null) {
                pageindex = Integer.parseInt(request.getParameter("page"));
            }
            List<Doctor> listAllDocBySpec = doctorDAO.getAllDoctorBySpec(spec); // get doctor by spec
            int totalDoc = listAllDocBySpec.size();
            System.out.println("total doc by spec: " + totalDoc);
            totalPage = totalDoc / 10;
            //get doctor by spec for pagination
            List<Doctor> listDocPaging;
            if (listAllDocBySpec.size() < 10) {
                totalPage = 1;
                listDocPaging = listAllDocBySpec;
            } else {
                int end = (pageindex * 10) < listAllDocBySpec.size() ? (pageindex * 10):listAllDocBySpec.size();
                listDocPaging = listAllDocBySpec.subList((pageindex - 1) * 10, end);
                if (totalDoc % 10 != 0) {
                    totalPage += 1;
                }
            }
            System.out.println("total page: " + totalPage);
            request.setAttribute("listDoc", listAllDocBySpec);
            request.setAttribute("pageIndex", pageindex);
            request.setAttribute("totalPage", totalPage);
            request.setAttribute("spec", spec);
            request.setAttribute("listDocSpec", listDoctorSpec);
            request.setAttribute("listDocPaging", listDocPaging);
            request.getRequestDispatcher("doctorBySpec.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("doctorList.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
