package vinmec.controller;

import dal.DoctorDAO;
import model.Doctor;
import uitility.Uti;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "DoctorDetailServlet", value = "/doctordetail")
public class DoctorDetailServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Uti u = new Uti();
        int docid = 1;
        if(request.getParameter("docid")!= null){
            docid = Integer.parseInt(request.getParameter("docid"));
        }
        String listUrl = "doctorList?page=";
        String filterUrl = "";
        DoctorDAO doctorDAO = new DoctorDAO();
        Doctor doc = doctorDAO.getDoctorById(docid);
        String docName = doc.getDoctorName();
        doc.setDoctorName(u.splitString(docName));
        String docPosition = doctorDAO.getPositionByDocid(docid);
        request.setAttribute("doctor",doc);
        request.setAttribute("position",docPosition);
        request.getRequestDispatcher("doctordetail.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
