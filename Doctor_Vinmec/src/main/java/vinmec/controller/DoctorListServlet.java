package vinmec.controller;

import dal.DoctorDAO;
import dal.GeneralDAO;
import model.*;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;


@WebServlet(name = "DoctorListServlet",value={"/doctorList"})
public class DoctorListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DoctorDAO doctorDAO = new DoctorDAO();
        GeneralDAO generalDAO = new GeneralDAO();
        //pagination
        final int pageSize = 10;
        int totalPage = 1;
        int pageIndex = 1;

        ArrayList<Doctor> listDoctor;
        ArrayList<Location> listLocation = generalDAO.getAllLocation();
        ArrayList<Speciality> listSpec = generalDAO.getSpeciality();
        ArrayList<Language> listLang = generalDAO.getAllLanguage();
        ArrayList<Title> listTitle = generalDAO.getTitle();
        ArrayList<Degree> lisDegree = generalDAO.getDegree();

        //filter
        String[] title = {};
        String[] lang = {};
        String[] spec = {};
        String[] deg = {};
        String[] location = {};
        if(request.getParameterValues("title") != null){
            title = request.getParameterValues("title");
        }
        if(request.getParameterValues("language") != null){
            lang = request.getParameterValues("language");
        }
        if(request.getParameterValues("speciality") != null){
            spec = request.getParameterValues("speciality");
        }
        if(request.getParameterValues("degree") != null){
            deg = request.getParameterValues("degree");
        }
        if(request.getParameterValues("location") != null){
            location = request.getParameterValues("location");
        }
        try {
            if (request.getParameter("page") != null) {
                pageIndex = Integer.parseInt(request.getParameter("page"));
            }
            int totalAmount = doctorDAO.getDocQuantaty();
            if(title.length!=0 ||spec.length!=0 || lang.length!=0 || deg.length!=0 || location.length!=0){
                totalAmount = doctorDAO.countTotalDoctor(title,deg,spec,lang,location);
            }
            totalPage = totalAmount / pageSize;
            if (totalAmount % pageSize != 0) {
                totalPage++;
            }
            if(totalAmount <= 10){
                totalPage = 1;
            }
        } catch (NumberFormatException ex) {
            PrintWriter out = response.getWriter();
            request.getRequestDispatcher("testList.jsp").forward(request, response);
            out.println("Page index must be a number, try enter again");
        }
        if(title.length==0 && spec.length==0 && lang.length==0 && deg.length==0 && location.length==0){
            listDoctor = doctorDAO.getAllListDoctorByPaging(pageIndex);
        }else{
            listDoctor = doctorDAO.filterDoctor2(pageIndex,title,deg,spec,lang,location);
        }
        ArrayList<Speciality> listDoctorSpec = doctorDAO.listDoctorSpecSidebar();
        request.setAttribute("pageIndex",pageIndex);
        request.setAttribute("totalPage", totalPage);
        request.setAttribute("listSpec", listSpec);
        request.setAttribute("listLang", listLang);
        request.setAttribute("listTitle",listTitle);
        request.setAttribute("listDegree",lisDegree);
        request.setAttribute("listDoc",listDoctor);
        request.setAttribute("listDocSpec",listDoctorSpec);
        request.setAttribute("listLocation",listLocation);
        request.getRequestDispatcher("doctorList.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
