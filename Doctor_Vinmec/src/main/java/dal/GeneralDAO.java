package dal;

import model.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class GeneralDAO extends DBContext{

    public ArrayList<Location> getAllLocation(){
        ArrayList<Location> list = new ArrayList<>();
        String sql = "SELECT * FROM vinmec.location;";
        try {
            PreparedStatement stm = conn.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Location l = new Location(rs.getInt(1), rs.getString(2), rs.getString(3));
                list.add(l);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public ArrayList<Speciality> getSpeciality(){
        ArrayList<Speciality> list = new ArrayList<>();
        String sql = "SELECT * FROM vinmec.speciality limit 22 offset 0";
        try {
            PreparedStatement stm = conn.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Speciality s = new Speciality(rs.getString(1), rs.getString(3));
                list.add(s);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public ArrayList<Language> getAllLanguage(){
        ArrayList<Language> list = new ArrayList<>();
        String sql = "SELECT * FROM vinmec.Language;";
        try {
            PreparedStatement stm = conn.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Language l = new Language(rs.getString(1), rs.getString(3));
                list.add(l);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public ArrayList<Degree> getDegree(){
        ArrayList<Degree> list = new ArrayList<>();
        String sql = "SELECT * FROM vinmec.degree;";
        try {
            PreparedStatement stm = conn.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Degree s = new Degree(rs.getString(1), rs.getString(2));
                list.add(s);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public ArrayList<Title> getTitle(){
        ArrayList<Title> list = new ArrayList<>();
        String sql = "SELECT * FROM vinmec.title";
        try {
            PreparedStatement stm = conn.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Title s = new Title(rs.getString(1), rs.getString(2));
                list.add(s);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

}
