package dal;

import model.Doctor;
import model.Speciality;
import uitility.Uti;

import javax.rmi.CORBA.Util;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class DoctorDAO extends DBContext {
    public ArrayList<Doctor> getAllListDoctorByPaging(int pageindex) {
        Uti uti = new Uti();
        ArrayList<Doctor> list = new ArrayList<>();
        String sql = "SELECT distinct di.id, di.doctor_name,\n" +
                "GROUP_CONCAT(DISTINCT deg.degree_name SEPARATOR ',') as 'degree',\n" +
                "t.title_name, \n" +
                "GROUP_CONCAT(DISTINCT s.spec_name SEPARATOR ',') as 'speciality',\n" +
                "d.workplace, di.intro,di.image, di.detail_link,\n" +
                "GROUP_CONCAT(DISTINCT l.language SEPARATOR ',') as 'language',\n" +
                "d.position\n" +
                "FROM doctor_info di\n" +
                "join doctor_degree dd on di.id = dd.docid join degree deg on dd.degreeid = deg.id\n" +
                "join doctor_title dt on di.id = dt.docid join title t on t.id = dt.titleid\n" +
                "join doctor_language dl on di.id = dl.docid join language l on dl.langid = l.id\n" +
                "join doctor_speciality dp on di.id = dp.docid join speciality s on dp.specid = s.id\n" +
                "join doctor d on d.id = di.id\n" +
                "group by di.id\n" +
                "limit 10 offset ?";
        try {
            PreparedStatement stm = conn.prepareStatement(sql);
            stm.setInt(1, (pageindex - 1) * 10);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                String intro = uti.cutStringByRegex(rs.getString(7), "Xem");
                Doctor d = new Doctor(
                        rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6),
                        intro, rs.getString(8), rs.getString(9),
                        rs.getString(10), rs.getString(11));
                list.add(d);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public ArrayList<Doctor> getDoctorHomePage() {
        ArrayList<Doctor> list = new ArrayList<>();
        String sql = "SELECT * FROM doctor LIMIT 4 OFFSET 15;";
        try {
            PreparedStatement stm = conn.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Doctor d = new Doctor(
                        rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6),
                        rs.getString(7), rs.getString(8), rs.getString(9),
                        rs.getString(10), rs.getString(11));
                list.add(d);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public Doctor getDoctorById(int docid) {
        String sql = "SELECT distinct di.id, di.doctor_name,\n" +
                "GROUP_CONCAT(DISTINCT deg.degree_name SEPARATOR ',') as 'degree',t.title_name, \n" +
                "GROUP_CONCAT(DISTINCT s.spec_name SEPARATOR ',') as 'speciality',d.workplace, di.intro,di.image, di.detail_link,\n" +
                "GROUP_CONCAT(DISTINCT l.language SEPARATOR ',') as 'language',d.position\n" +
                "FROM doctor_info di\n" +
                "join doctor_degree dd on di.id = dd.docid join degree deg on dd.degreeid = deg.id\n" +
                "join doctor_title dt on di.id = dt.docid join title t on t.id = dt.titleid\n" +
                "join doctor_language dl on di.id = dl.docid join language l on dl.langid = l.id\n" +
                "join doctor_speciality dp on di.id = dp.docid join speciality s on dp.specid = s.id\n" +
                "join doctor d on d.id = di.id\n" +
                "group by di.id having (di.id = ?)";
        try {
            PreparedStatement stm = conn.prepareStatement(sql);
            stm.setInt(1, docid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Doctor d = new Doctor(
                        rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6),
                        rs.getString(7), rs.getString(8), rs.getString(9),
                        rs.getString(10), rs.getString(11));
                return d;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public int getDocQuantaty() {
        String sql = "select count(*) from doctor_info";
        try {
            PreparedStatement stm = conn.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return 0;
    }

    public String getPositionByDocid(int docid) {
        String sql = "SELECT position FROM doctor where doctor_id = ?;";
        try {
            PreparedStatement stm = conn.prepareStatement(sql);
            stm.setInt(1, docid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public ArrayList<Speciality> listDoctorSpecSidebar(){
        ArrayList<Speciality> list = new ArrayList<>();
        try{

            String sql = "SELECT s.spec_name, s.spec_code, count(distinct di.id) as number\n" +
                    "FROM doctor_info di\n" +
                    "join doctor_speciality dp on di.id = dp.docid join speciality s on dp.specid = s.id\n" +
                    "group by s.spec_name";
//            System.out.println(sql);
            PreparedStatement stm = conn.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()){
                Speciality s = new Speciality(rs.getString(1),rs.getString(2),rs.getInt(3));
                list.add(s);
            }
            return list;
        }catch (Exception e){

        }
        return null;
    }

    public ArrayList<Doctor> getAllDoctorBySpec(String spec){
        ArrayList<Doctor> list = new ArrayList<>();
        try{
            String sql = "SELECT distinct di.id, di.doctor_name,\n" +
                    "GROUP_CONCAT(DISTINCT deg.degree_name SEPARATOR ',') as 'degree',t.title_name, \n" +
                    "GROUP_CONCAT(DISTINCT s.spec_name SEPARATOR ',') as 'speciality',d.workplace, di.intro,di.image, di.detail_link,\n" +
                    "GROUP_CONCAT(DISTINCT l.language SEPARATOR ',') as 'language',d.position\n" +
                    "FROM doctor_info di\n" +
                    "join doctor_degree dd on di.id = dd.docid join degree deg on dd.degreeid = deg.id\n" +
                    "join doctor_title dt on di.id = dt.docid join title t on t.id = dt.titleid\n" +
                    "join doctor_language dl on di.id = dl.docid join language l on dl.langid = l.id\n" +
                    "join doctor_speciality dp on di.id = dp.docid join speciality s on dp.specid = s.id\n" +
                    "join doctor d on d.id = di.id\n" +
                    "where (di.id > 0)\n" +
                    "and(s.spec_code = ?)\n" +
                    "group by di.id\n";
            PreparedStatement stm = conn.prepareStatement(sql);
            stm.setString(1,spec);
            System.out.println(sql);
            ResultSet rs = stm.executeQuery();
            Uti u = new Uti();
            while (rs.next()){
                String intro = u.cutStringByRegex(rs.getString(7), "Xem");
                Doctor d = new Doctor(
                        rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6),
                        intro, rs.getString(8), rs.getString(9),
                        rs.getString(10), rs.getString(11));
                list.add(d);
            }
            return list;
        }catch (Exception e){

        }
        return null;
    }

    public int countTotalDoctor(String[] title, String[] degree, String[] spec, String[] lang, String[] location) {
        try {
            String sql = "with t as(\n" +
                    "SELECT distinct di.id, di.doctor_name,\n" +
                    "GROUP_CONCAT(DISTINCT deg.degree_name SEPARATOR ',') as 'degree',t.title_name, \n" +
                    "GROUP_CONCAT(DISTINCT s.spec_name SEPARATOR ',') as 'speciality',d.workplace, di.intro,di.image, di.detail_link,\n" +
                    "GROUP_CONCAT(DISTINCT l.language SEPARATOR ',') as 'language',loc.location_name\n" +
                    "FROM doctor_info di\n" +
                    "join doctor_degree dd on di.id = dd.docid join degree deg on dd.degreeid = deg.id\n" +
                    "join doctor_title dt on di.id = dt.docid join title t on t.id = dt.titleid\n" +
                    "join doctor_language dl on di.id = dl.docid join language l on dl.langid = l.id\n" +
                    "join doctor_speciality dp on di.id = dp.docid join speciality s on dp.specid = s.id\n" +
                    "join doctor_location dloc on di.id = dloc.docid join location loc on dloc.locationid = loc.locationid\n" +
                    "join doctor d on d.id = di.id\n" +
                    "where (di.id > 0)\n";
            if (title.length > 0) {
                sql += "and(";
                for (int i = 0; i < title.length; i++) {
                    sql += "t.title_name = '" + title[i] + "'";
                    if (i != title.length - 1) {
                        sql += " or ";
                    }
                }
                sql += ")\n";
            }
            if (degree.length > 0) {
                sql += "and(";
                for (int i = 0; i < degree.length; i++) {
                    sql += "deg.degree_name = '" + degree[i] + "'";
                    if (i != degree.length - 1) {
                        sql += " or ";
                    }
                }
                sql += ")\n";
            }
            if (spec.length > 0) {
                sql += "and(";
                for (int i = 0; i < spec.length; i++) {
                    sql += "speciality = '" + spec[i] + "'";
                    if (i != spec.length - 1) {
                        sql += " or ";
                    }
                }
                sql += ")\n";
            }
            if (lang.length > 0) {
                sql += "and(";
                for (int i = 0; i < lang.length; i++) {
                    sql += "l.language = '" + lang[i] + "'";
                    if (i != lang.length - 1) {
                        sql += " or ";
                    }
                }
                sql += ")\n" ;
            }
            if (location.length > 0) {
                sql += "and(";
                for (int i = 0; i < location.length; i++) {
                    sql += "loc.location_code = '" + location[i] + "'";
                    if (i != location.length - 1) {
                        sql += " or ";
                    }
                }
                sql += ")\n";
            }
            sql += "group by di.id)\n" +
                    "select count(id) from t;";
            PreparedStatement stm = conn.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        }
        catch(Exception e){

        }
        return 0;
    }
    public ArrayList<Doctor> filterDoctor2(int pageindex, String[] title, String[] degree, String[] spec, String[] lang, String[] location) {
        ArrayList<Doctor> list = new ArrayList<>();
        try {
            String sql = "SELECT distinct di.id, di.doctor_name,\n" +
                    "GROUP_CONCAT(DISTINCT deg.degree_name SEPARATOR ',') as 'degree',t.title_name, \n" +
                    "GROUP_CONCAT(DISTINCT s.spec_name SEPARATOR ',') as 'speciality',d.workplace, di.intro,di.image, di.detail_link,\n" +
                    "GROUP_CONCAT(DISTINCT l.language SEPARATOR ',') as 'language',loc.location_name\n" +
                    "FROM doctor_info di\n" +
                    "join doctor_degree dd on di.id = dd.docid join degree deg on dd.degreeid = deg.id\n" +
                    "join doctor_title dt on di.id = dt.docid join title t on t.id = dt.titleid\n" +
                    "join doctor_language dl on di.id = dl.docid join language l on dl.langid = l.id\n" +
                    "join doctor_speciality dp on di.id = dp.docid join speciality s on dp.specid = s.id\n" +
                    "join doctor_location dloc on di.id = dloc.docid join location loc on dloc.locationid = loc.locationid\n" +
                    "join doctor d on d.id = di.id\n" +
                    "where (di.id > 0)";
            if (title.length > 0) {
                sql += "and(";
                for (int i = 0; i < title.length; i++) {
                    sql += "t.title_name = '" + title[i] + "'";
                    if (i != title.length - 1) {
                        sql += " or ";
                    }
                }
                sql += ")\n";
            }
            if (degree.length > 0) {
                sql += "and(";
                for (int i = 0; i < degree.length; i++) {
                    sql += "deg.degree_name = '" + degree[i] + "'";
                    if (i != degree.length - 1) {
                        sql += " or ";
                    }
                }
                sql += ")\n";
            }
            if (spec.length > 0) {
                sql += "and(";
                for (int i = 0; i < spec.length; i++) {
                    sql += "s.spec_name like '" + spec[i] + "'";
                    if (i != spec.length - 1) {
                        sql += " or ";
                    }
                }
                sql += ")\n";
            }
            if (lang.length > 0) {
                sql += "and(";
                for (int i = 0; i < lang.length; i++) {
                    sql += "l.language = '" + lang[i] + "'";
                    if (i != lang.length - 1) {
                        sql += " or ";
                    }
                }
                sql += ")\n";
            }
            if (location.length > 0) {
                sql += "and(";
                for (int i = 0; i < location.length; i++) {
                    sql += "loc.location_code = '" + location[i] + "'";
                    if (i != location.length - 1) {
                        sql += " or ";
                    }
                }
                sql += ")\n";
            }
            sql += "group by di.id\n" +
                    "limit 10 offset ?;";
            System.out.println(sql);
            PreparedStatement stm = conn.prepareStatement(sql);
            stm.setInt(1, (pageindex - 1) * 10);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Doctor d = new Doctor(
                        rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6),
                        rs.getString(7), rs.getString(8), rs.getString(9),
                        rs.getString(10), rs.getString(11));
                list.add(d);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public static void main(String[] args) {
        Uti u = new Uti();
        DoctorDAO doctorDAO = new DoctorDAO();
        ArrayList<Doctor> listDoctor = doctorDAO.getAllListDoctorByPaging(1);
        for (Doctor d : listDoctor) {
            //System.out.println(d.getWorkplace());
        }
        Doctor d = doctorDAO.getDoctorById(1);
//        System.out.println(d.getImage());

//        String[] title = {"Giáo sư","Phó Giáo sư"};
        String[] degree = {"Bác sĩ","Tiến sĩ","Thạc sĩ"};
//        String[] location = {};
        String[] spec = {"Nhi","Thẩm mỹ"};
        String[] title = {};
//        String[] degree = {};
        String[] location = {"ha-noi", "hai-phong"};
//        String[] spec = {};
        String[] lang = {"Tiếng Anh", "Tiếng Đức"};

        int list = doctorDAO.countTotalDoctor(title,degree,spec,lang,location);
        //ArrayList<Speciality> listSpec = doctorDAO.listDoctorSpecSidebar();
        System.out.println(list);
//        for(Speciality s : listSpec){
//            System.out.println(s.getSpecid());
//        }

    }
}
