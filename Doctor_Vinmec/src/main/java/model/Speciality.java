package model;

public class Speciality {
    private String specid;
    private String specName;
    private int doctorAmount;

    public Speciality(String specid, String specName) {
        this.specid = specid;
        this.specName = specName;
    }

    public Speciality(String specName,String specCode, int docNumber) {
        this.specName = specName;
        this.specid = specCode;
        this.doctorAmount = docNumber;
    }

    public String getSpecid() {
        return specid;
    }

    public void setSpecid(String specid) {
        this.specid = specid;
    }

    public String getSpecName() {
        return specName;
    }

    public void setSpecName(String specName) {
        this.specName = specName;
    }

    public int getDoctorAmount() {
        return doctorAmount;
    }

    public void setDoctorAmount(int doctorAmount) {
        this.doctorAmount = doctorAmount;
    }
}
