package model;

public class Title {
    private String titleid;
    private String title;

    public Title(String titleid, String title) {
        this.titleid = titleid;
        this.title = title;
    }

    public String getTitleid() {
        return titleid;
    }

    public void setTitleid(String titleid) {
        this.titleid = titleid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
