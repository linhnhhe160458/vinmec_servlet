package model;

public class Language {
    private String langid;
    private String language;

    public Language(String langid, String language) {
        this.langid = langid;
        this.language = language;
    }

    public String getLangid() {
        return langid;
    }

    public void setLangid(String langid) {
        this.langid = langid;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
