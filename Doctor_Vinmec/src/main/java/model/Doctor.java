package model;

public class Doctor {
    private int doctorID;
    private String doctorName;
    private String degree;
    private String title;
    private String speciality;
    private String workplace;
    private String intro;
    private String image;
    private String detailLink;
    private String language;
    private String position;

//    public Doctor(int docid, String docname, String img, String intro, String detailLink,String position, String degree, String title, String language,String workplace){
//        this.doctorID = docid;
//        this.doctorName = docname;
//        this.workplace = workplace;
//        this.degree = degree;
//        this.title = title;
//        this.intro = intro;
//        this.image = img;
//        this.detailLink = detailLink;
//        this.language = language;
//        this.position = position;
//    }

    public Doctor(int doctorID, String doctorName, String degree, String title, String speciality, String workplace, String intro, String image, String detailLink, String language, String position) {
        this.doctorID = doctorID;
        this.doctorName = doctorName;
        this.degree = degree;
        this.title = title;
        this.speciality = speciality;
        this.workplace = workplace;
        this.intro = intro;
        this.image = image;
        this.detailLink = detailLink;
        this.language = language;
        this.position = position;
    }
    public Doctor(int doctorID, String doctorName, String image, String intro, String detailLink) {
        this.doctorID = doctorID;
        this.doctorName = doctorName;
        this.intro = intro;
        this.image = image;
        this.detailLink = detailLink;
    }

    public int getDoctorID() {
        return doctorID;
    }

    public void setDoctorID(int doctorID) {
        this.doctorID = doctorID;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWorkplace() {
        return workplace;
    }

    public void setWorkplace(String workplace) {
        this.workplace = workplace;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDetailLink() {
        return detailLink;
    }

    public void setDetailLink(String detailLink) {
        this.detailLink = detailLink;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

}
