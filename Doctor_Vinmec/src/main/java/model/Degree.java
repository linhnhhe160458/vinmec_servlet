package model;

public class Degree {
    private String degreeid;
    private String degreeName;

    public Degree(String degreeid, String degreeName) {
        this.degreeid = degreeid;
        this.degreeName = degreeName;
    }

    public String getDegreeid() {
        return degreeid;
    }

    public void setDegreeid(String degreeid) {
        this.degreeid = degreeid;
    }

    public String getDegreeName() {
        return degreeName;
    }

    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }
}
