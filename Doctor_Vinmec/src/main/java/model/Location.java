package model;

public class Location {
    private int locationid;
    private String locationName;
    private String locationCode;

    public Location(int locationid, String locationName, String locationCode) {
        this.locationid = locationid;
        this.locationName = locationName;
        this.locationCode = locationCode;
    }

    public int getLocationid() {
        return locationid;
    }

    public void setLocationid(int locationid) {
        this.locationid = locationid;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }
}
